# Válvula Antiretorno para uso con máscaras 
Esta válvula se debería utilizar con los adaptadores charlotte en la entrada de Oxígeno para evitar que la exhalación del paciente suba por el tubo de O2. Aprovecha las dos membranas que se deben quitar del interior de la máscara de decathlon.
Se ha desarrollado en respuesta la demanda de algunos hospitales que nos indicaban el problema de retorno pese al alto flujo de Oxígeno.

<img src="https://i.gyazo.com/80283a0830787d7f4e26821ee4351623.jpg" height="300"> 

## Materiales de impresión
Todas las impresiones se deben de hacer con filamento de PLA (si puede ser sin pigmento mejor), es lo que se ha probado y validado por varios hospitales.
Las resinas no sirven por su toxicidad.

## Montaje y Uso

Si se van a conectar dos tomas de Oxígeno o una toma de oxígeno y una bolsa de reservorio, mirar la sección Dave que integra la válvula en el conector T.

Imprimir las dos piezas, insertar la membrana en la cruzilla y unir las dos piezas con presión. Usar en la entrada de Oxígeno just antes de la máscara.

<img src="https://i.gyazo.com/5eaed649992450e8d0c678b96f736ff1.jpg" height="200">

<img src="https://gyazo.com/c48a9e77e93adf75c81bc2225e01dc41" height="200">

#### Modelo

Vista Transveral:

<img src="https://i.gyazo.com/00e650d917441e0a73855f13900e213d.png" height="200">

### Fichero para imprimir
VERSION ACTUAL VALIDADA: **v1**

[FICHERO DE IMPRESION A](https://gitlab.com/ma-d-kers/library/-/raw/master/Valvula_Antiretorno_V1/A_Valvula_Antiretorno_V1.stl?inline=false)

[FICHERO DE IMPRESION B](https://gitlab.com/ma-d-kers/library/-/raw/master/Valvula_Antiretorno_V1/B_Valvula_Antiretorno_V1.stl?inline=false)


## Impresión

Imprimir la cruzilla hacia abajo y la otra pieza en la siguente configuración:

<img src="https://i.gyazo.com/24e2a89c45ad11482aa4253f6522f8f9.png" height="200">

## Parámetros de impresión

Como norma general y teniendo en cuenta donde se van a usar esas piezas se requiere imprimir con una calidad aceptable, siendo preferible generar menos piezas de buena calidad a muchas piezas de peor calidad que pueden presentar problemas diversos.

Estos son los parámetros de impresión que se están usando para validar las piezas cuando se termina el diseño:

* Filamento: PLA 1.75mm
* Altura de capa: 0.2
* Grosor de pared: 1.6
* Relleno: 80%
* Soportes: No hacen falta

Aunque usar los mismos parámetros debería asegurar repetibilidad y calidad esto puede variar dependiendo de la impresora y el filamento que use cada maker por consiguiente se deberán ajustar para conseguir los resultados óptimos.

Las distintas pruebas han demostrado que usar una balsa de adherencia evita problemas de piezas despegadas de las camas.

## Calibración

### Configurar la expansión horizontal

Es fundamental realizar este proceso antes de comenzar a imprimir piezas porque las distintas configuraciones de impresoras combinadas con el uso de distintas marcas de filamento puede generar desviaciones en la impresión que afecten al ajuste final de la pieza con la máscara. 

Imprimir [Calibre_Charlotte_Reforzada.stl](Calibre_Charlotte_Reforzada.stl)


Es muy importante que utilices estas piezas de calibrado y no otras obtenidas en otro canal porque han sido diseñadas específicamente para corregir una holgura detectada en el modelo STL Charlotte que vas a imprimir.

Tras imprimirlas piezas tendrás que comprobar que el cilindro entre justo en la anilla.

No es necesario que pase completamente. pero si que entre ajustado haciendo algo de presión. Si lo podéis meter como en la foto siguiente, será suficiente:

![](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/images/calibre_xh_1.png)

Si no hay forma de que el cilindro entre dentro de la anilla se debe modificar en el cura el parámetro Expansión horizontal:
* Si el cilindro no entraba en la anilla hay que disminuir el valor que haya (se aceptan valores negativos). Se puede ir bajando de décima de milímetro en décima de milímetro.
* Si el cilindro pasa holgado por la anilla habrá que incrementar el valor.
* Es normal que el valor resulte entre -0.2 y +0.2 (se admiten ajustes más precisos, por ejemplo -0.16)

Cuando se consiga un parámetro que permita entrar el cilindro ajustado dentro de la anilla se mantiene para utilizarlo para imprimir.


